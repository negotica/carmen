@extends('layouts.card')
@section('card-header', 'Edit car')
@section('card')

    <form method="POST" enctype="multipart/form-data" action="{{route('cars.update',[$car->id])}}">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="created" class="col-md-4 col-form-label text-md-right">Created at :</label>
            <label for="created"
                   class="col-md-4 col-form-label text-md-right">{{ $car->created_at->format('d-m-y H:i:s')}}</label>
        </div>

        <div class="form-group row">
            <label for="brand" class="col-md-4 col-form-label text-md-right">{{ __('Brand') }}</label>

            <div class="col-md-6">
                <input id="brand" type="text"
                       class="form-control @error('brand') is-invalid @enderror" name="brand"
                       value="{{ $car->brand }}" autocomplete="brand" autofocus>
            </div>
        </div>

        <div class="form-group row">
            <label for="model"
                   class="col-md-4 col-form-label text-md-right">{{ __('Model') }}
            </label>
            <div class="col-md-6">
                <input id="model" type="text"
                       class="form-control @error('model') is-invalid @enderror"
                       name="model" value="{{ $car->model }}" autocomplete="model">
            </div>
        </div>

        <div class="form-group row">
            <label for="license_plate"
                   class="col-md-4 col-form-label text-md-right">{{ __('License Plate') }}
            </label>
            <div class="col-md-6">
                <input id="license_plate" type="text"
                       class="form-control @error('license_plate') is-invalid @enderror"
                       name="license_plate" value="{{ $car->license_plate }}" autocomplete="license_plate">
            </div>
        </div>

        <div class="form-group row">
            <label for="photo"
                   class="col-md-4 col-form-label text-md-right">{{ __('Photo') }}
            </label>
            <div class="col-md-6">
                <input id="photo" type="file"
                       class="form-control @error('photo') is-invalid @enderror"
                       name="photo" value="{{ $car->photo }}">
                <img class="picture" src="{{asset('storage/'.$car->photo) }}"/>

            </div>
        </div>

        <div class="form-group row">
            <label for="registration_certificate"
                   class="col-md-4 col-form-label text-md-right">{{ __('Registration Certificate') }}
            </label>
            <div class="col-md-6">
                <input id="registration_certificate" type="file"
                       class="form-control @error('registration_certificate') is-invalid @enderror"
                       name="registration_certificate" value="{{ $car->registration_certificate }}">
                <img class="picture" src="{{asset('storage/'.$car->registration_certificate) }}"/>

            </div>
        </div>


        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
                <a href="{{route('cars.index')}}" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
@endsection
