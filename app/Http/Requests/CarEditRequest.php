<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => [
                'required',
                'max:30',
                'min:2',
            ],
            'model' => [
                'required',
                'max:30',
                'min:2',
            ],
            'license_plate' => [
                'required',
                'size:8',
            ],
            'photo' => [
                'image',
                'max:2048',

            ],
            'registration_certificate' => [
                'image',
                'max:2048',
            ],
        ];
    }
}
