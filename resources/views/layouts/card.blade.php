@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">@yield('card-header')</div>
                <div class="notification">
                    <ul id="error_message">
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="card-body">

                    @yield('card')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
