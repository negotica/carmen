@extends('layouts.card')
@section('card-header', 'Dashboard')
@section('card')

    @if (Auth::user()->role === ('manager'))
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Admin panel</h5>
                        <p class="card-text">For changing users, cars, bookings and rides</p>
                        <a href="admin">Admin panel</a>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection
