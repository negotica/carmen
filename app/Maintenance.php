<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
