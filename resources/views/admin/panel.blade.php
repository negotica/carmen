@extends('layouts.card')
@section('card-header', 'Admin Panel')
@section('card')
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">User panel</h5>
                    <p class="card-text">For adding, removing and changing users</p>
                    <a href="/users">Users</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Car panel</h5>
                    <p class="card-text">For adding, removing and changing cars</p>
                    <a href="/cars">Cars</a>
                </div>
            </div>
        </div>
    </div>
@endsection
