@extends('layouts.card')
@section('card-header')
    Car panel
    <div class="float-right"><a href="{{route('cars.create')}}">Create car</a></div>
@endsection
@section('card')

    <table id="car-table">
        <thead>
        <tr>
            <th id="brand">Brand</th>
            <th id="model">Model</th>
            <th id="license-plate">Plate no.</th>
            <th id="photo">Photo</th>
            <th id="registration-certificate">Certificate</th>
            <th id="adjust">Adjust</th>
            <th id="delete">Delete</th>

        </tr>
        </thead>

        <tbody>
        @foreach ($cars as $car)
            <tr>
                <td>{{ $car->brand }}</td>
                <td>{{ $car->model }}</td>
                <td>{{ $car->license_plate }}</td>
                <td><a href="{{asset('storage/'.$car->photo) }}"> Photo</a> </td>
                <td><a href="{{asset('storage/'.$car->registration_certificate) }}">Certificate</a> </td>

                <td>
                    <a href="{{route('cars.edit',[$car->id])}}" class="btn btn-secondary">
                        <i class="far fa-edit"> </i>
                    </a>
                </td>
                <td>
                    <form action="{{route('cars.destroy',[$car->id])}}" method="POST">
                        @csrf @method('DELETE')
                        <button id="delete" type="submit" class="btn btn-danger"
                                onclick="return confirm('Do you really want to delete this car?');">
                            <i class="far fa-trash-alt"> </i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#car-table').DataTable();
        });
    </script>

@endsection
