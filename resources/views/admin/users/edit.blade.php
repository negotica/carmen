@extends('layouts.card')
@section('card-header', 'Edit user')
@section('card')

    <form method="POST" action="{{route('users.update',[$user->id])}}">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="created" class="col-md-4 col-form-label text-md-right">Created at :</label>
            <label for="created"
                   class="col-md-4 col-form-label text-md-right">{{ $user->created_at->format('d-m-y H:i:s') }}</label>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text"
                       class="form-control @error('name') is-invalid @enderror" name="name"
                       value="{{ $user->name }}" autocomplete="name" autofocus>

            </div>
        </div>

        <div class="form-group row">
            <label for="email"
                   class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email"
                       class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ $user->email }}" autocomplete="email">

            </div>
        </div>

        <div class="form-group row">
            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

            <div class="col-md-6">

                <select id="role" type="text"
                        class="form-control @error('role') is-invalid @enderror" name="role"
                        autocomplete="role" autofocus>
                    @if (($user->role) === 'manager')
                        <option value="manager">Manager</option>
                        <option value="user">User</option>
                    @else
                        <option value="user">User</option>
                        <option value="manager">Manager</option>
                    @endif
                </select>


            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
                <a href="{{route('users.index')}}" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
@endsection
