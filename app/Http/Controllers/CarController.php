<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use App\Http\Requests\CarEditRequest;
use Illuminate\Support\Facades\Storage;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::all();

        return view('admin.cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        $car = new Car();

        $car->brand = request('brand');
        $car->model = request('model');
        $car->license_plate = request('license_plate');
        $car->photo = request()->file('photo')->store('cars', 'public');
        $car->registration_certificate = request()->file('registration_certificate')->store('certificates', 'public');
        $car->save();

        return redirect()->route('cars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        return view('admin.cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarEditRequest $request, Car $car)
    {
        $car->brand = request('brand');
        $car->model = request('model');
        $car->license_plate = request('license_plate');

        if ($request->has('photo')) {
            Storage::disk('public')->delete($car->photo);

            $car->photo = request()->file('photo')->store('cars', 'public');
        }

        if ($request->has('registration_certificate')) {
            Storage::disk('public')->delete($car->registration_certificate);

            $car->registration_certificate = request()->file('registration_certificate')->store('certificates', 'public');
        }

        $car->save();

        return redirect()->route('cars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        Storage::disk('public')->delete($car->photo);
        Storage::disk('public')->delete($car->registration_certificate);

        $car->delete();

        return back();
    }
}
