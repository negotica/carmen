@extends('layouts.card')
@section('card-header')
            User panel
            <div class="float-right"><a href="{{route('users.create')}}">Create users</a></div>
@endsection
@section('card')

        <table id="user-table">
            <thead>
            <tr>
                <th id="name">Name</th>
                <th id="email">Email</th>
                <th id="role">Role</th>
                <th id="adjust">Adjust</th>
                <th id="delete">Delete</th>
            </tr>
            </thead>

            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role }}</td>

                    <td>
                        <a href="{{route('users.edit',[$user->id])}}" class="btn btn-secondary">
                            <i class="far fa-edit"> </i>
                        </a>
                    </td>
                    <td>
                        <form action="{{route('users.destroy',[$user->id])}}" method="POST">
                            @csrf @method('DELETE')
                            <button id="delete" type="submit" class="btn btn-danger"
                                    onclick="return confirm('Do you really want to delete this user?');">
                                <i class="far fa-trash-alt"> </i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script>
            $(document).ready(function () {
                $('#user-table').DataTable();
            });
        </script>

@endsection
