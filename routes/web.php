<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');

Auth::routes([
    'register' => false,
    'verify' => false,
]);

Route::middleware(['auth', 'manager'])->group(function () {
    Route::resource('users', 'UserController')->except(['show']);
    Route::resource('cars', 'CarController');

    Route::get('admin', function () {
        return view('admin.panel');
    });
});

Route::middleware(['auth'])->group(function () {
    Route::resource('home', 'HomeController');
});
