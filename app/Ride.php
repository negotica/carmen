<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
