<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function maintenances()
    {
        return $this->hasMany('App\Maintenance');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
